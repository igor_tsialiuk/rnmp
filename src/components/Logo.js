import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { FontAwesome as Icon } from '@expo/vector-icons';
import { Font } from 'expo';

class Logo extends Component < {} > {
    constructor() {
        super();
        this.state = {
            font: ''
        };
    }
    async componentDidMount() {
        await Font.loadAsync({
            'Oswald-Regular': require('../fonts/Oswald/Oswald-Regular.ttf')
        });
        this.setState({ font: 'Oswald-Regular' });
    }
    render() {
        const { iconStyle, textStyle, containerStyle } = this.props;

        return (
            <View style={ [styles.logo, containerStyle] }>
                <Icon style={ [styles.logoIcon, iconStyle] } name='chevron-left'/>
                <Text style={ [styles.logoText, textStyle, { fontFamily: this.state.font }] }>epamer</Text>
                <Icon style={ [styles.logoIcon, iconStyle] } name='chevron-right'/>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    logo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    logoText: {
        color: '#464547',
    },
    logoIcon: {
        color: '#39C2D7',
    },
});

export default Logo;
