import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';

class ButtonHighlight extends Component < {} > {
    render() {
        const { buttonStyle, buttonTextStyle, text, onPress } = this.props;

        return (
            <TouchableHighlight underlayColor='#FFFFFF' onPress={onPress}>
                <View style={ [styles.button, buttonStyle] }>
                    <Text style={ [styles.buttonText, buttonTextStyle] }>{text.toUpperCase()}</Text>
                </View>
            </TouchableHighlight>
        );
    }
};

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 22,
    },
});

export default ButtonHighlight;
