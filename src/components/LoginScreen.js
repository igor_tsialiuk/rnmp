import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight } from 'react-native';

import ButtonHighlight from './ButtonHighlight';
import Logo from './Logo';

class LoginScreen extends Component < {} > {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: ''
        };
        this.submit = this.submit.bind(this);
    }
    submit(e) {
        if (this.state.login && this.state.password) {
            alert('ok');
        }
    }
    render() {
        return (
            <View style={ styles.container }>
                <Logo
                    containerStyle={{marginBottom: 70}}
                    textStyle={{fontSize: 72}}
                    iconStyle={{marginTop: 22, fontSize: 36}}/>
                <TextInput
                    onEndEditing={this.submit}
                    autoFocus={true}
                    value={this.state.login}
                    onChangeText={(text) => this.setState({login: text})}
                    placeholder='Username'
                    placeholderTextColor='#999999'
                    underlineColorAndroid='transparent'
                    style={styles.input} />
                <TextInput
                    onEndEditing={this.submit}
                    secureTextEntry={true}
                    value={this.state.password}
                    onChangeText={(text) => this.setState({password: text})}
                    placeholder='********'
                    placeholderTextColor='#999999'
                    underlineColorAndroid='transparent'
                    style={styles.input}/>
                <ButtonHighlight
                    buttonStyle={{backgroundColor: '#A3C644', height: 50, marginTop: 20}}
                    buttonTextStyle={{color: '#FFFFFF'}}
                    onPress={this.submit}
                    text='Login'/>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingBottom: 60,
        justifyContent: 'flex-end',
    },
    input: {
        fontSize: 24,
        height: 50,
        color: '#1A9CB0',
        marginBottom: 30,
        paddingLeft: 20,
        borderColor: '#999999',
        borderWidth: 1,
        paddingVertical: 0,
    },
});

export default LoginScreen;
